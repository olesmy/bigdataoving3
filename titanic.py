# -*- coding: utf-8 -*-


import pandas as pd
from sklearn.model_selection import train_test_split  
from sklearn import tree
from sklearn.tree import DecisionTreeClassifier  
from sklearn.metrics import classification_report  


# Load CSV into dataframe
df = pd.read_csv('data/titanic.csv', sep=',')

# Remove entries where 'Age' is unknown
df = df[df['Age'].notnull()]

# Sex column is currently text, convert it to numerical values
sexes = {'male': 0, 'female': 1}
df.replace(sexes, inplace=True)

# Isolate the 'Survived' column as that's what we want to predict
y = df['Survived']
x = df.drop('Survived', axis=1)

# Split our data into 20% test data, and 80% training data
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2)

# Grow tree, using max_depth and/or min_impurity_decrease (gini) to manipulate depth
classifier = DecisionTreeClassifier(min_impurity_decrease=0.02, max_depth=5)  
classifier.fit(x_train, y_train)

# Test the tree and log results
y_pred = classifier.predict(x_test) 
print(classification_report(y_test, y_pred))  

# Save the tree to a dotfile so we can visualize it later
dotfile = open("./dtree2.dot", 'w')
ditfile = tree.export_graphviz(classifier, out_file = dotfile, feature_names = x.columns, class_names=["Survived", "Not Survive"])
dotfile.close()

