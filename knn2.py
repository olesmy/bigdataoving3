import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from sklearn import manifold

# Load CSV into a dataframe
df = pd.read_csv('data/dishes.csv', sep=';')

# Reshape into a wide dataframe
df = df.pivot_table(index='UserName', values='Score', columns='Dish')


def distance(col1, col2):
    """Computes the euclidean distance between two columns"""
    return np.linalg.norm((col1 - col2).fillna(0))


# Compute the euclidean distance between rows.
# Since the function uses columns, flip the table
df = df.T
distances = df.apply(lambda col2: df.apply(lambda col1: distance(col1, col2)))

mds = manifold.MDS(n_components=2, dissimilarity='precomputed', random_state=1)
results = mds.fit(distances)
coords = results.embedding_

# Plot points as circles
plt.subplots_adjust(bottom=0.1)
plt.scatter(coords[:, 0], coords[:, 1], marker='o')

# Plot 'User' labels
for label, x, y in zip(df.columns.values, coords[:, 0], coords[:, 1]):
    plt.annotate(
        label,
        xy=(x, y), xytext=(-20, 20),
        textcoords='offset points', ha='right', va='bottom',
        bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
        arrowprops=dict(arrowstyle='->', connectionstyle='arc3,rad=0'))
plt.show()


# Find nearest neighbors by orderering by distance from "me"
me = 'olesmy'
neighbors = distances[me].drop(me, axis=0)  # Remove "me" from neighbors

# Use only k-nearest neighbors
k = 3
orderedNeighbors = neighbors.sort_values()  # Sort neighbours by distance asc.
nearestNeighbors = orderedNeighbors[0:k]  # Select the k first entries
print("Send dinner invitations to:")
print(nearestNeighbors)
