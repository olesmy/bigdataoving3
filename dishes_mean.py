# -*- coding: utf-8 -*-


import pandas as pd
import matplotlib.pyplot as plt

# Load CSV into a dataframe
df = pd.read_csv('data/dishes.csv', sep=';')

# Find the mean for each dish
mean = df.groupby(['Dish']).mean()          
labels = mean.index.values
values = mean['Score'].values
print(mean.head(20))
print(labels)
print(values)

plt.title('Dishes vs Score')
plt.ylabel("Score")
plt.xlabel("Dishes")
plt.bar(labels, values)
plt.xticks(rotation=90)
plt.show()
